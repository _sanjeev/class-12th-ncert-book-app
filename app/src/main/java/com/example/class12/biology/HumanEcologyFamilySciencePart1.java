package com.example.class12.biology;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class HumanEcologyFamilySciencePart1 extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(HumanEcologyFamilySciencePart1.this,topics,"Human Ecology"));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Human Ecology");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","Unit I - Work Livelihood & Career"));
        topics.add(new Topics("2","Unit II - Nutrition, Food Science & Technology"));
        topics.add(new Topics("3","Unit III - Public Nutrition & Health"));
        topics.add(new Topics("4","Unit IV - Catering & Food Service Management"));
        topics.add(new Topics("5","Unit V - Food Processing & Technology"));
        topics.add(new Topics("6","Unit VI - Food Quality & Food Safety"));
        topics.add(new Topics("7","Unit VII - Human Development & Family Studies"));
        topics.add(new Topics("8","Unit VIII - Guidance & Counselling"));
        topics.add(new Topics("9","Unit IX - Special Education & Support Services"));
        topics.add(new Topics("10","Unit X - Management Of Support Services..."));


        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(HumanEcologyFamilySciencePart1.this,topics,"Human Ecology"));


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
