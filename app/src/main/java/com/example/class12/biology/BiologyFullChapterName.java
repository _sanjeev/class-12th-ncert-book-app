package com.example.class12.biology;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class BiologyFullChapterName extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(BiologyFullChapterName.this,topics,"Biology"));
        }

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Biology");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","Chapter 1 - Reproduction in Organisms"));
        topics.add(new Topics("2","Chapter 2 - Sexual Reproduction in Flowering Plants"));
        topics.add(new Topics("3","Chapter 3  Human Reproduction"));
        topics.add(new Topics("4","Chapter 4 - Reproductive Health"));
        topics.add(new Topics("5","Chapter 5 - Principles of Inheritance and Variation"));
        topics.add(new Topics("6","Chapter 6 - Molecular Basis of Inheritance"));
        topics.add(new Topics("7","Chapter 7 - Evolution"));
        topics.add(new Topics("8","Chapter 8 - Human Health and Disease"));
        topics.add(new Topics("9","Chapter 9 - Strategies for Enhancement in Food Production"));
        topics.add(new Topics("10","Chapter 10 - Microbes in Human Welfare"));
        topics.add(new Topics("11","Chapter 11 - Biotechnology Principles and Processes"));
        topics.add(new Topics("12","Chapter 12 - Biotechnology and its Applications"));
        topics.add(new Topics("13","Chapter 13 - Organisms and Populations"));
        topics.add(new Topics("14","Chapter 14 - Ecosystem"));
        topics.add(new Topics("15","Chapter 15 - Biodiversity and Conservation"));
        topics.add(new Topics("16","Chapter 16 - Environmental Issues"));


        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(BiologyFullChapterName.this,topics,"Biology"));



    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(BiologyFullChapterName.this).unregisterReceiver(receiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        LocalBroadcastManager.getInstance(BiologyFullChapterName.this).unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
