package com.example.class12.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.class12.R;
import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import java.io.File;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class PDFViewActivity extends AppCompatActivity {
    Toolbar toolbar;
    PDFView pdfView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_p_d_f_view);

        toolbar = findViewById(R.id.toolbar);

        pdfView = findViewById(R.id.pdfView);

        File root = new File(Environment.getExternalStorageDirectory(), "Class_12_Ncert_Book");
        if (!root.exists()) {
            root.mkdirs();
//            if (mInterstitialAd.isLoaded()){
////                mInterstitialAd.show();
////            }
        }

        String temp = getIntent().getStringExtra("fileURI");
        String str = temp.replace(".pdf","");
        toolbar.setTitle(str);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final File file = new File(root, temp);

        PDFView.Configurator configurator = pdfView.fromFile(file);

        configurator.enableSwipe(true);
        configurator.enableDoubletap(true);
        configurator.scrollHandle(new DefaultScrollHandle(this));
        pdfView.setSwipeVertical(true);
        configurator.enableAnnotationRendering(true);

        configurator.load();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}