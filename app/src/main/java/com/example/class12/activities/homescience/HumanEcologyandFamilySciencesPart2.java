package com.example.class12.activities.homescience;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class HumanEcologyandFamilySciencesPart2 extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(HumanEcologyandFamilySciencesPart2.this,topics,"Human Ecology and Family Sciences Part 2"));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Human Ecology and Family Sciences Part 2");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","Chapter 11 - Design for Fabric and Apparel"));
        topics.add(new Topics("2","Chapter 12 - Fashion Design and Merchandising"));
        topics.add(new Topics("3","Chapter 13 - Production and Quality Control in the Garment Industry"));
        topics.add(new Topics("4","Chapter 14 - Textile Conservation in Museums"));
        topics.add(new Topics("5","Chapter 15 - Care and Maintenance of Fabrics in Instutions"));
        topics.add(new Topics("6","Chapter 16 - Human Resources Management"));
        topics.add(new Topics("7","Chapter 17 - Hospitality Management"));
        topics.add(new Topics("8","Chapter 18 - Ergonomics and Designing of Interior and..."));
        topics.add(new Topics("9","Chapter 19 - Event Management"));
        topics.add(new Topics("10","Chapter 20 - Consumer Education and Protection"));
        topics.add(new Topics("11","Chapter 21 - Development Communication and Journalism"));
        topics.add(new Topics("12","Chapter 22 - Advocacy"));
        topics.add(new Topics("13","Chapter 23 - Media Management, Design and Production"));
        topics.add(new Topics("14","Chapter 24 - Corporate Communication and Public Relations"));
        topics.add(new Topics("15","Chapter 25 - Management of Development Programmes"));



        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(HumanEcologyandFamilySciencesPart2.this,topics,"Human Ecology and Family Sciences Part 2"));


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


