package com.example.class12.activities.hindi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.adapter.SanskritAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class Aroh extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(Aroh.this,topics,"Aroh"));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Aroh");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","Chapter 1 - हरिवंश राय बच्चन (आत्मपरिचय)"));
        topics.add(new Topics("2","Chapter 2 - आलोक धन्वा (पतंग)"));
        topics.add(new Topics("3","Chapter 3 - कुँवर नारायण (कविता के बहाने)"));
        topics.add(new Topics("4","Chapter 4 - रघुवीर सहाय (कैमरे में बंद अपाहिज)"));
        topics.add(new Topics("5","Chapter 5 - गजानन माधव मुक्तिबोध (सहर्ष स्वीकारा है)"));
        topics.add(new Topics("6","Chapter 6 - शमशेर बहादुर सिंह (उषा)"));
        topics.add(new Topics("7","Chapter 7 - सूर्यकांत त्रिपाठी 'निराला' (बादल राग)"));
        topics.add(new Topics("8","Chapter 8 - तुलसीदास (कवितावली)"));
        topics.add(new Topics("9","Chapter 9 - फ़िराक गोरखपुरी (रुबाइयाँ)"));
        topics.add(new Topics("10","Chapter 10 - उमाशंकर जोशी (छोटा मेरा खेत)"));
        topics.add(new Topics("11","Chapter 11 - महादेवी वर्मा (भक्तिन)"));
        topics.add(new Topics("12","Chapter 12 - जैनेन्द्र कुमार (बाज़ार दर्शन)"));
        topics.add(new Topics("13","Chapter 13 - धर्मवीर भारती (काले मेघा पानी दे)"));
        topics.add(new Topics("14","Chapter 14 - फणीश्वर नाथ रेणु (पहलवान की ढोलक)"));
        topics.add(new Topics("15","Chapter 15 - विष्णु खरे (चार्ली चैप्लिन यानी हम सब)"));
        topics.add(new Topics("16","Chapter 16 - रज़िया सज्जाद ज़हीर (नमक)"));
        topics.add(new Topics("17","Chapter 17 - हजारी प्रसाद द्विदेदी (शिरीष के फूल)"));
        topics.add(new Topics("18","Chapter 18 - बाबा साहेब भीमराव आंबेडकर (श्रम विभाजन और जाति-प्रथा)"));




        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(Aroh.this,topics,"Aroh"));


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
