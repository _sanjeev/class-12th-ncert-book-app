package com.example.class12.activities.sociology;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class SocialChangeandDevelopmentInIndia extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(SocialChangeandDevelopmentInIndia.this,topics,"Social Change & Development in India"));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Social Change & Development in India");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","Chapter 1 - Structural Change"));
        topics.add(new Topics("2","Chapter 2 - Cultural Change"));
        topics.add(new Topics("3","Chapter 3 - The Story of Indian Democracy"));
        topics.add(new Topics("4","Chapter 4 - Change and Development in Rural Society"));
        topics.add(new Topics("5","Chapter 5 - Change and Development in Industrial Society"));
        topics.add(new Topics("6","Chapter 6 - Globalisation and Social Change"));
        topics.add(new Topics("7","Chapter 7 - Mass Media and Communications"));
        topics.add(new Topics("8","Chapter 8 - Social Movements"));


        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(SocialChangeandDevelopmentInIndia.this,topics,"Social Change & Development in India"));


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
