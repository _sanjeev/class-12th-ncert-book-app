package com.example.class12.activities.hindi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.adapter.SanskritAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class Antra extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(Antra.this,topics,"Antra"));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Antra");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","Chapter 1 - जयशंकर प्रसाद"));
        topics.add(new Topics("2","Chapter 2 - सूर्यकांत त्रिपाठी 'निराला'"));
        topics.add(new Topics("3","Chapter 3 - सच्चिदानंद हीरानंद वात्स्यायन अज्ञेय"));
        topics.add(new Topics("4","Chapter 4 - केदारनाथ सिंह"));
        topics.add(new Topics("5","Chapter 5 - विष्णु खरे"));
        topics.add(new Topics("6","Chapter 6 - रघुवीर सहाय"));
        topics.add(new Topics("7","Chapter 7 - तुलसीदास"));
        topics.add(new Topics("8","Chapter 8 - मलिक मुहम्मद जायसी"));
        topics.add(new Topics("9","Chapter 9 - विद्यापति"));
        topics.add(new Topics("10","Chapter 10 - केशवदास"));
        topics.add(new Topics("11","Chapter 11 - घनानंद"));
        topics.add(new Topics("12","Chapter 12 - रामचंद्र शुक्ल"));
        topics.add(new Topics("13","Chapter 13 - पंडित चंद्रधर शर्मा गुलेरी"));
        topics.add(new Topics("14","Chapter 14 - ब्रजमोहन व्यास"));
        topics.add(new Topics("15","Chapter 15 - फणीश्वरनाथ 'रेणु'"));
        topics.add(new Topics("16","Chapter 16 - भीष्म साहनी"));
        topics.add(new Topics("17","Chapter 17 - असगर वजाहत"));
        topics.add(new Topics("18","Chapter 18 - निर्मल वर्मा"));
        topics.add(new Topics("19","Chapter 19 - रामविलास शर्मा"));
        topics.add(new Topics("20","Chapter 20 - ममता कालिया"));
        topics.add(new Topics("21","Chapter 21 - हजारी प्रसाद द्विवेदी"));



        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(Antra.this,topics,"Antra"));


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
