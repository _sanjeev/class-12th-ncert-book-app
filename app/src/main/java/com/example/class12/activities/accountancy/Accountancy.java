package com.example.class12.activities.accountancy;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.AccountancyAdapter;
import com.example.class12.adapter.PhysicsAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class Accountancy extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView recyclerView;
    List<Topics> list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Accountancy");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView = findViewById(R.id.recyclerview);

        list = new ArrayList<>();
        list.add(new Topics("C","Computerised Accounting"));
        list.add(new Topics("1","Accountancy Part 1"));
        list.add(new Topics("2","Accountancy Part 1"));

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setAdapter(new AccountancyAdapter(this,list));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
