package com.example.class12.activities.hindi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.adapter.SanskritAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class Antral extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(Antral.this,topics,"Antral"));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Antral");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","Chapter 1 - सूरदास की झोंपड़ी"));
        topics.add(new Topics("2","Chapter 2 - आरोहण"));
        topics.add(new Topics("3","Chapter 3 - बिस्कोहर की माटी"));
        topics.add(new Topics("4","Chapter 4 - अपना मालवा-खाऊ-उजाड़ू सभ्यता में"));



        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(Antral.this,topics,"Antral"));


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
