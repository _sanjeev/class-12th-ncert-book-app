package com.example.class12.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.class12.R;
import com.example.class12.adapter.TopicAdapter;
import com.example.class12.pojo.Topics;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class NavigationDrawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    DrawerLayout drawerLayout;
    NavigationView navigationView;
    Toolbar toolbar;
    RecyclerView recyclerView;
    List<Topics> topics;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.navigation_view);
        recyclerView = findViewById(R.id.recyclerview);
        toolbar = findViewById(R.id.toolbar);
        setTitle("CLASS 12 NCERT BOOK");
        setSupportActionBar(toolbar);

        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle=new ActionBarDrawerToggle(this, drawerLayout, toolbar,R.string.open_drawer,R.string.close_drawer);
        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        topics = new ArrayList<>();
        topics.add(new Topics("M","Mathematics"));
        topics.add(new Topics("P","Physics"));
        topics.add(new Topics("A","Accountancy"));
        topics.add(new Topics("S","Sanskrit"));
        topics.add(new Topics("H","Hindi"));
        topics.add(new Topics("E","English"));
        topics.add(new Topics("B","Biology"));
        topics.add(new Topics("H","History"));
        topics.add(new Topics("G","Geography"));
        topics.add(new Topics("S","Sociology"));
        topics.add(new Topics("P","Psychology"));
        topics.add(new Topics("C","Chemistry"));
        topics.add(new Topics("P","Political Science"));
        topics.add(new Topics("E","Economics"));
        topics.add(new Topics("B","Business Studies"));
        topics.add(new Topics("H","Heritage Crafts"));
        topics.add(new Topics("N","New Age Graphics"));
        topics.add(new Topics("H","Home Science"));

        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        recyclerView.setAdapter(new TopicAdapter(this,topics));
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.feedback_id:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto","abc@gmail.com", null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Learn Java Programming for Beginners");
                emailIntent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;

            case R.id.shareapp_id:
                share(this);
                break;

            case R.id.rateus_id:
                rateUs(this);
                break;
            case R.id.privacypolicy_id:
                break;

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else{
            //super.onBackPressed();
            int num = new Random().nextInt(50) + 1;
            if (num % 2 != 0)
                showRateUs();
            else
                super.onBackPressed();
        }

    }

    private void showRateUs() {
        this.dialog = new AlertDialog.Builder(this).create();
        this.dialog.setTitle("Please Rate This App.");
        this.dialog.setMessage("it would encourage us to come up with more content like this. ⭐⭐⭐⭐⭐");
        this.dialog.setCancelable(true);
        this.dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Rating", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                rateUs(NavigationDrawer.this);
            }
        });
        this.dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAffinity();
            }
        });
        this.dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "✔ Already Rated.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(NavigationDrawer.this, "THANK YOU FOR ⭐⭐⭐⭐⭐", Toast.LENGTH_SHORT).show();
                finishAffinity();
            }
        });
        this.dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.share:
                share(this);
                break;

            case R.id.rate:
                rateUs(this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}