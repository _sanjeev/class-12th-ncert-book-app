package com.example.class12.activities.english;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.adapter.ChapterAdapter;
import com.example.class12.pojo.Topics;

import java.util.ArrayList;
import java.util.List;

import static com.example.class12.AppController.rateUs;
import static com.example.class12.AppController.share;

public class Kaliedoscope extends AppCompatActivity {

    Toolbar toolbar;
    List<Topics> topics=new ArrayList<>();
    RecyclerView rv_chapter;

    BroadcastReceiver receiver=new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            rv_chapter.setAdapter(new ChapterAdapter(Kaliedoscope.this,topics,"Kaleidoscope"));
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mathematics_part1);


        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Kaleidoscope");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rv_chapter = findViewById(R.id.recyclerview);

        rv_chapter.setLayoutManager(new LinearLayoutManager(this));

        topics.add(new Topics("1","1. Sell My Dreams - Short Stories"));
        topics.add(new Topics("2","2. Eveline - Short Stories"));
        topics.add(new Topics("3","3. A Wedding In Brownsville - Short Stories"));
        topics.add(new Topics("4","4. Tomorrow - Short Stories"));
        topics.add(new Topics("5","5. One Centimetre - Short Stories"));
        topics.add(new Topics("6","1. A Lecture Upon The Shadow - Poerty"));
        topics.add(new Topics("7","2. Poems By Milton - Poerty"));
        topics.add(new Topics("8","3. Poems By Blake - Poerty"));
        topics.add(new Topics("9","4. Kubla Khan Or A Vision In A Dream A Fragment - Poerty"));
        topics.add(new Topics("10","5. Trees - Poerty"));
        topics.add(new Topics("11","6. The Wild Swans At Coole - Poerty"));
        topics.add(new Topics("12","7. Time & Time Again - Poerty"));
        topics.add(new Topics("13","8. Blood - Poerty"));
        topics.add(new Topics("14","1 - Non-Fiction - Freedom"));
        topics.add(new Topics("15","2 - Non-Fiction - The Mark on the Wall"));
        topics.add(new Topics("16","3 - Non-Fiction - Film-Making"));
        topics.add(new Topics("17","4 - Non-Fiction - Why the Novel Matters"));
        topics.add(new Topics("18","5 - Non-Fiction - The Argumentative Indian"));
        topics.add(new Topics("19","6 - Non-Fiction - On Science Fiction"));
        topics.add(new Topics("20","1 - Drama - Chandalika"));
        topics.add(new Topics("21","2 - Drama - Broken Images"));


        registerReceiver(receiver,new IntentFilter("file_downloaded"));

        rv_chapter.setAdapter(new ChapterAdapter(Kaliedoscope.this,topics,"Kaleidoscope"));


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        unregisterReceiver(receiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.share) {
            share(this);

        } else if (id == R.id.rate) {
            rateUs(this);
        }
        if(id== android.R.id.home) {

            finish();
            unregisterReceiver(receiver);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
