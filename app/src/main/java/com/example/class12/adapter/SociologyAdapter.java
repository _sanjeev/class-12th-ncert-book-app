package com.example.class12.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.activities.sanskrit.Bhaswati;
import com.example.class12.activities.sanskrit.Saswati;
import com.example.class12.activities.sociology.IndianSociety;
import com.example.class12.activities.sociology.SocialChangeandDevelopmentInIndia;
import com.example.class12.pojo.Topics;

import java.util.List;
import java.util.Random;

import static com.example.class12.AppController.colors;

public class SociologyAdapter extends RecyclerView.Adapter<SociologyAdapter.ViewHolder> {

    Context context;
    List<Topics> topic;
    Random random;

    public SociologyAdapter(Context context, List<Topics> topics) {
        this.context = context;
        this.topic = topics;
        random = new Random();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Topics topics = topic.get(position);
        holder.tv.setText(topics.getTitle());
        holder.tvNo.setText(topics.getId());

        int rand = random.nextInt(colors.length);
        final Bitmap image = Bitmap.createBitmap(60,60, Bitmap.Config.ARGB_8888);
        image.eraseColor(colors[rand]);
        holder.imageView.setImageBitmap(image);

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position==0){
                    context.startActivity(new Intent(context, IndianSociety.class));
                }if(position==1){
                    context.startActivity(new Intent(context, SocialChangeandDevelopmentInIndia.class));
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return topic.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        TextView tv,tvNo;
        ImageView imageView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tv = itemView.findViewById(R.id.tv);
            tvNo = itemView.findViewById(R.id.tv_no);
            imageView = itemView.findViewById(R.id.img);
            cardView = itemView.findViewById(R.id.lt_root);

        }

    }

}
