package com.example.class12.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.R;
import com.example.class12.activities.accountancy.Accountancy;
import com.example.class12.activities.businessstudies.BusinessStudies;
import com.example.class12.activities.chemistry.Chemistry;
import com.example.class12.activities.economics.Economics;
import com.example.class12.activities.english.English;
import com.example.class12.activities.geography.Geography;
import com.example.class12.activities.heritagecraft.HeritageCrafts;
import com.example.class12.activities.hindi.Hindi;
import com.example.class12.activities.history.History;
import com.example.class12.activities.homescience.HomeScience;
import com.example.class12.activities.mathematics.MainActivity;
import com.example.class12.activities.newagegraphics.NewAgeGraphics;
import com.example.class12.activities.physics.Physics;
import com.example.class12.activities.politicalScience.PoliticalScience;
import com.example.class12.activities.psychology.Phychology;
import com.example.class12.activities.sanskrit.Sanskrit;
import com.example.class12.activities.sociology.sociology;
import com.example.class12.biology.Biology;
import com.example.class12.pojo.Topics;

import java.util.List;
import java.util.Random;

import static com.example.class12.AppController.colors;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.ViewHolder> {

    Context context;
    List<Topics> list;
    private Random random;

    public TopicAdapter(Context context, List<Topics> list) {
        this.context = context;
        this.list = list;
        random = new Random();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Topics topics = list.get(position);
        holder.tv.setText(topics.getTitle());
        holder.tvNo.setText(topics.getId());

        int randomNum = random.nextInt((colors.length));
        final Bitmap image = Bitmap.createBitmap(60, 60, Bitmap.Config.ARGB_8888);
        image.eraseColor(colors[randomNum]);
        holder.imageView.setImageBitmap(image);

        holder.ltRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(position==0){
                    context.startActivity(new Intent(context, MainActivity.class));
                }
                if(position==1){
                    context.startActivity(new Intent(context, Physics.class));
                }if(position==2){
                    context.startActivity(new Intent(context, Accountancy.class));
                }if(position==3){
                    context.startActivity(new Intent(context, Sanskrit.class));
                }if (position==4){
                    context.startActivity(new Intent(context, Hindi.class));
                }if(position==5){
                    context.startActivity(new Intent(context, English.class));
                }if (position==6){
                    context.startActivity(new Intent(context, Biology.class));
                }if (position==7){
                    context.startActivity(new Intent(context, History.class));
                }if (position==8){
                    context.startActivity(new Intent(context, Geography.class));
                }if (position==9){
                    context.startActivity(new Intent(context, sociology.class));
                }if (position==10){
                    context.startActivity(new Intent(context, Phychology.class));
                }if (position==11){
                    context.startActivity(new Intent(context, Chemistry.class));
                }if (position==12){
                    context.startActivity(new Intent(context, PoliticalScience.class));
                }if (position==13){
                    context.startActivity(new Intent(context, Economics.class));
                }if (position==14){
                    context.startActivity(new Intent(context, BusinessStudies.class));
                }if (position==15){
                    context.startActivity(new Intent(context, HeritageCrafts.class));
                }if (position==16){
                    context.startActivity(new Intent(context, NewAgeGraphics.class));
                }if (position==17){
                    context.startActivity(new Intent(context, HomeScience.class));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        CardView ltRoot;
        ImageView imageView;
        TextView tvNo,tv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ltRoot = itemView.findViewById(R.id.lt_root);
            imageView = itemView.findViewById(R.id.img);
            tvNo = itemView.findViewById(R.id.tv_no);
            tv = itemView.findViewById(R.id.tv);
        }
    }
}
