package com.example.class12.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.example.class12.AppController;
import com.example.class12.R;
import com.example.class12.activities.Permission;
import com.example.class12.pojo.Topics;

import java.io.File;
import java.util.List;
import java.util.Random;

import static com.example.class12.AppController.colors;

public class ChapterAdapter extends RecyclerView.Adapter<ChapterAdapter.ViewHolder>{
    Context context;
    List<Topics> topics;
    private Random rand;
    String subject;

    public ChapterAdapter(Context context, List<Topics> topics,String subject) {
        this.context = context;
        this.topics = topics;
        rand = new Random();
        this.subject = subject;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Topics topic = topics.get(position);
        holder.title.setText(topic.getTitle());
        holder.tvNo.setText(topic.getId());

        int randomNum = rand.nextInt((colors.length));
        Bitmap image = Bitmap.createBitmap(60, 60, Bitmap.Config.ARGB_8888);
        image.eraseColor(colors[randomNum]);
        holder.imageView.setImageBitmap(image);

        final String fName=topic.getTitle()+".pdf";

        File root = new File(Environment.getExternalStorageDirectory(), "Class_12_Ncert_Book");
        if (!root.exists()) {
            root.mkdirs();

        }

        final File f = new File(root, fName);
        if (f.exists() && !f.isDirectory()) {
            holder.ltRoot.setBackgroundColor(Color.WHITE);
            holder.del.setVisibility(View.VISIBLE);
            holder.del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmDelete(f,position);
                }
            });
        }
        else{
            holder.ltRoot.setBackgroundColor(Color.parseColor("#e9e9e9"));
            holder.del.setVisibility(View.GONE);
        }

        holder.ltRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!AppController.checkPermission(context)){
                    context.startActivity(new Intent(context, Permission.class));
                    return;
                }
                AppController.downloadPdf(context,subject,topic.getTitle());

            }
        });
    }

    private void confirmDelete(final File f, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Sure ! Delete File?");
        builder.setMessage("क्या आप फ़ाइल को हटाना चाहते हैं");
        builder.setIcon(R.drawable.delete);
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                f.delete();
                notifyItemChanged(position);
                context.sendBroadcast(new Intent("file_downloaded"));
            }
        });
        builder.setNegativeButton("CANCEL", null);
        builder.show();
    }

    @Override
    public int getItemCount() {
        return topics.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView title,tvNo;
        ImageView del,imageView;
        LinearLayout ltRoot;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.title);
            tvNo = itemView.findViewById(R.id.tv_no);
            del = itemView.findViewById(R.id.del);
            imageView = itemView.findViewById(R.id.imageView);
            ltRoot = itemView.findViewById(R.id.lt_root);
        }
    }
}
