package com.example.class12;

import android.Manifest;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import com.example.class12.activities.PDFViewActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;

public class AppController extends Application {
    public static int[] colors;
    public static void downloadPdf(final Context context, String subject, String name) {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        final String fName=name+".pdf";

        StorageReference ref = storage.getReference().child(subject).child(fName);

        File root = new File(Environment.getExternalStorageDirectory(), "Class_12_Ncert_Book");
        if (!root.exists()) {
            root.mkdirs();
        }



        final File f = new File(root, name + ".pdf");
        final Uri uri = FileProvider.getUriForFile(context,context.getApplicationContext().getPackageName() + ".fileprovider", f);

        if (f.exists() && !f.isDirectory()) {
            AppController.openPDF(context, uri, f,fName);
            return;
        }
        if (!AppController.isInternetAvailable(context)) {

            showNoInternet(context);
            return;
        }

        try {
            f.createNewFile();
        } catch (IOException e) {
            Log.e("ex", e.toString());
        }
        final ProgressDialog dialog = new ProgressDialog(context);
        dialog.setTitle("Downloading File..");
        dialog.setIcon(R.drawable.download);
        dialog.setMessage("Please Wait..! It will take few second to complete downloading.");
        dialog.setIndeterminate(true);
        dialog.setProgressStyle(0);
        dialog.setCancelable(false);

        dialog.show();

        ref.getFile(f).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                // Local temp file has been created
                context.sendBroadcast(new Intent("file_downloaded"));
                AppController.openPDF(context, uri, f,fName);
                dialog.dismiss();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                f.delete();
                Toast.makeText(context, "Downloading Failed", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });

    }

    private static void showNoInternet(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("First Time Need Internet !");
        builder.setMessage("पहली बार इंटरनेट की आवश्यकता है फ़ाइल डाउनलोड के लिये,फिर आप हमेशा ऑफ़लाइन उपयोग करेंगे");
        builder.setIcon(R.drawable.no_internet);
        builder.setPositiveButton("OK", null);
        builder.show();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        initializeColors();
    }

    private void initializeColors() {
        colors = new int[7];
        colors[0] = Color.parseColor("#ff9800");
        colors[1] = Color.parseColor("#03a9f4");
        colors[2] = Color.parseColor("#e91e63");
        colors[3] = Color.parseColor("#009688");
        colors[4] = Color.parseColor("#8bc34a");
        colors[5] = Color.parseColor("#ffc107");
        colors[6] = Color.parseColor("#0000ff");
    }

    public static boolean checkPermission(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && context.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        } else
            return true;
    }



    public static void openPDF(Context context, Uri uri, File file, String name) {
        Intent intent;
        intent = new Intent(context, PDFViewActivity.class).putExtra("fileURI", name);
        context.startActivity(intent);

    }

    public static boolean isInternetAvailable(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService(CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void share(Context context) {
        try {
            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "NCERT BOOK & SOLUTIONS");
            String shareMessage = "Check out this amazing app";
            //shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
            shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
            context.startActivity(Intent.createChooser(shareIntent, "Choose One"));
        } catch (Exception e) {
            //e.toString();
        }
    }

    public static void rateUs(Context context) {
        context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID)));
    }


}
